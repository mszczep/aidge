/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_HARDMAXIMPL_H_
#define AIDGE_CPU_OPERATOR_HARDMAXIMPL_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Hardmax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// class Hardmax_Op;

// We template the kernel on the input and ouput types
// Change the arguments according to the inputs and outputs of the operator
class HardmaxImplForward_cpu
    : public Registrable<HardmaxImplForward_cpu, std::tuple<DataType, DataType>, void(const typename Hardmax_Op::Attrs&, const std::vector<DimSize_t>&, const void*, void*)> {
};
class HardmaxImplBackward_cpu
    : public Registrable<HardmaxImplBackward_cpu, std::tuple<DataType, DataType>, void(const typename Hardmax_Op::Attrs&, const std::vector<DimSize_t>&, const void*, void*)> {
};
// Then we declare the Impl class for the operator
class HardmaxImpl_cpu : public OperatorImpl {
public:
    HardmaxImpl_cpu(const Hardmax_Op& op) : OperatorImpl(op) {}

    static std::unique_ptr<HardmaxImpl_cpu> create(const Hardmax_Op& op) {
        return std::make_unique<HardmaxImpl_cpu>(op);
    }

    void forward() override;
};

// Finally we create the Registrar for the operator implementation in which we specify the backend cpu
namespace {
static Registrar<Hardmax_Op> registrarHardmaxImpl_cpu("cpu", Aidge::HardmaxImpl_cpu::create);
}
}  // namespace Aidge

#endif /* _AIDGE_CPU_OPERATOR_HARDMAXIMPL_H_ */