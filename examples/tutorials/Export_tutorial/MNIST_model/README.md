# MNIST model
This repository is a tool for the Aidge demonstration (Import ONNX -> Graph transformation -> Inference).
```generate_model.sh``` export an ONNX of a Multi Layer Perceptron learned on MNIST with the following steps :
- Create a virtual envieronnement with pytorch CPU & ONNX
- Call torch_MLP.py that download the MNIST data, learn an MLP on MNIST digit recognition & export the model as an ONNX
- Remove the virtual environnement & MNIST data