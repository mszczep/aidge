{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Post Training Quantization with AIDGE\n",
    "\n",
    "#### What is Network Quantization ?\n",
    "\n",
    "Deploying large Neural Network architectures on embedded targets can be a difficult task as they often require billions of floating operations per inference.\n",
    "\n",
    "To address this problem, several techniques have been developed over the past decades in order to reduce the computational load and energy consumption of those inferences. Those techniques include Pruning, Compression, Quantization and Distillation.\n",
    "\n",
    "In particular, Post Training Quantization (PTQ) consists in taking an already trained network, and replacing the costly floating-point MADD by their integer counterparts. The use of Bytes instead of Floats also leads to a smaller memory bandwidth.\n",
    "\n",
    "While this process can seem trivial, the naive approach consisting only in rounding the parameters and activations doesn't work in practice. Instead, we want to normalize the network in order to optimize the ranges of parameters and values propagated inside the network, before applying quantization.\n",
    "\n",
    "#### The Quantization Pipeline\n",
    "\n",
    "The PTQ algorithm consists in a 3 steps pipeline: \n",
    "\n",
    "- First we optimize the parameter ranges by propagating the scaling coefficients in the network. \n",
    "- Secondly, we compute the activation values over an input dataset, and insert the scaling nodes. \n",
    "- Finally, we quantize the network by reconfiguring the scaling nodes according to the desired precision.\n",
    "\n",
    "![alt text](./ptq_diagram.png)\n",
    "\n",
    "\n",
    "#### Doing the PTQ with AIDGE\n",
    "\n",
    "This notebook shows how to perform PTQ of a Convolutional Network, trained on the MNIST dataset.\n",
    "\n",
    "The tutorial is constructed as follows :\n",
    "\n",
    "- Setup of the AIDGE environment\n",
    "- Loading of the model and example inferences\n",
    "- Evaluation of the trained model accuracy\n",
    "- Post Training Quantization and test inferences\n",
    "- Evaluation of the quantized model accuracy\n",
    "\n",
    "As we will observe in this notebook, we get zero degradation of the accuracy for a 8-bits PTQ. \n",
    "\n",
    "\n",
    "Let's begin !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Environment setup ...\n",
    "\n",
    "We need numpy for manipulating the inputs, matplotlib for visualization purposes, and gzip to uncompress the numpy dataset.\n",
    "\n",
    "Then we want to import the aidge modules :\n",
    "\n",
    "- the core module contains everything we need to manipulate the graph.\n",
    "- the backend module allows us to perform inferences using the CPU.\n",
    "- the onnx module allows us to load the pretrained model (stored in an onnx file).\n",
    "- the quantization module encaplsulate the Post Training Quantization algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gzip\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import aidge_core\n",
    "import aidge_onnx\n",
    "import aidge_backend_cpu\n",
    "import aidge_quantization\n",
    "\n",
    "print(\" Available backends : \", aidge_core.Tensor.get_available_backends())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, let's define the configurations of this script ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NB_SAMPLES  = 100\n",
    "NB_BITS     = 8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's load and visualize some samples ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samples = np.load(gzip.GzipFile('assets/mnist_samples.npy.gz', \"r\"))\n",
    "labels  = np.load(gzip.GzipFile('assets/mnist_labels.npy.gz',  \"r\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(10):\n",
    "    plt.subplot(1, 10, i + 1)\n",
    "    plt.axis('off')\n",
    "    plt.tight_layout()\n",
    "    plt.imshow(samples[i], cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Importing the model in AIDGE ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aidge_model = aidge_onnx.load_onnx(\"assets/ConvNet.onnx\", verbose=False)\n",
    "aidge_core.remove_flatten(aidge_model) # we want to get rid of the 'flatten' nodes ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setting up the AIDGE scheduler ...\n",
    "\n",
    "In order to perform inferences with AIDGE we need to setup a scheduler. But before doing so, we need to create a data producer node and connect it to the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Insert the input producer\n",
    "input_node = aidge_core.Producer([1, 1, 28, 28], \"XXX\")\n",
    "input_node.add_child(aidge_model)\n",
    "aidge_model.add(input_node)\n",
    "\n",
    "# Set up the backend\n",
    "aidge_model.set_datatype(aidge_core.DataType.Float32)\n",
    "aidge_model.set_backend(\"cpu\")\n",
    "\n",
    "# Create the Scheduler\n",
    "scheduler = aidge_core.SequentialScheduler(aidge_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running some example inferences ...\n",
    "\n",
    "Now that the scheduler is ready, let's perform some inferences. To do so we first declare a utility function that will prepare and set our inputs, propagate them and retreive the outputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def propagate(model, scheduler, sample):\n",
    "    # Setup the input\n",
    "    sample = np.reshape(sample, (1, 1, 28, 28))\n",
    "    input_tensor = aidge_core.Tensor(sample)\n",
    "    input_node.get_operator().set_output(0, input_tensor)\n",
    "    # Run the inference\n",
    "    scheduler.forward()\n",
    "    # Gather the results\n",
    "    output_node = model.get_output_nodes().pop()\n",
    "    output_tensor = output_node.get_operator().get_output(0)\n",
    "    return np.array(output_tensor)\n",
    "\n",
    "print('\\n EXAMPLE INFERENCES :')\n",
    "for i in range(10):\n",
    "    output_array = propagate(aidge_model, scheduler, samples[i])\n",
    "    print(labels[i] , ' -> ', np.round(output_array, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Computing the model accuracy ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_accuracy(model, samples, labels):\n",
    "    acc = 0\n",
    "    for i, x in enumerate(samples):\n",
    "        y = propagate(model, scheduler, x)\n",
    "        if labels[i] == np.argmax(y):\n",
    "            acc += 1\n",
    "    return acc / len(samples)\n",
    "\n",
    "accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)\n",
    "print(f'\\n MODEL ACCURACY : {accuracy * 100:.3f}%')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Quantization dataset creation ...\n",
    "\n",
    "We need to convert a subset of our Numpy samples into AIDGE tensors, so that they can be used to compute the activation ranges."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tensors = []\n",
    "for sample in samples[0:NB_SAMPLES]:\n",
    "    sample = np.reshape(sample, (1, 1, 28, 28))\n",
    "    tensor = aidge_core.Tensor(sample)\n",
    "    tensors.append(tensor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Applying the PTQ to the model ...\n",
    "\n",
    "Now that everything is ready, we can call the PTQ routine ! Note that after the quantization we need to update the scheduler."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aidge_quantization.quantize_network(aidge_model, NB_BITS, tensors)\n",
    "\n",
    "scheduler = aidge_core.SequentialScheduler(aidge_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running some quantized inferences ...\n",
    "\n",
    "Now that our network is quantized, what about testing some inferences ? Let's do so, but before, we need not to forget that our 8-bit network expect 8-bit inputs ! We thus need to rescale the input tensors ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaling = 2**(NB_BITS-1)-1\n",
    "for i in range(NB_SAMPLES):\n",
    "    samples[i] = np.round(samples[i] * scaling)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now perform our quantized inferences ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('\\n EXAMPLE QUANTIZED INFERENCES :')\n",
    "for i in range(10):\n",
    "    input_array = np.reshape(samples[i], (1, 1, 28, 28))\n",
    "    output_array = propagate(aidge_model, scheduler, input_array)\n",
    "    print(labels[i] , ' -> ', np.round(output_array, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Computing the quantized accuracy ...\n",
    "\n",
    "Just as we've done for the initial network, we can compute the quantized model accuracy ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accuracy = compute_accuracy(aidge_model, samples[0:NB_SAMPLES], labels)\n",
    "print(f'\\n QUANTIZED MODEL ACCURACY : {accuracy * 100:.3f}%')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Work is done !\n",
    "\n",
    "We see that a 8-bit PTQ does not affect the accuracy of our model ! This result shows that a proper quantization algorithm can be used to deploy a Neural Network on very small devices, where manipulating bytes is optimal. We encourage you to run this notebook again with even more aggressive quantization values !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('That\\'s all folks !')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_aidge2",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
