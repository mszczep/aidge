{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Graph Regular Expression\n",
    "Graph Regular expression is a powerful tool inspired by regular expressions. It is designed to express complex graph structures, much like regular expressions do for character strings.\n",
    "\n",
    "This tutorial demonstrate the usage of the Graph Regular Expression to perform graph matching in Aidge.\n",
    "\n",
    "It is organized as follows : \n",
    "- Section Requirements covers the requirements to run this tutorial\n",
    "- Section Graph Regex Flow explains the user pipeline to use Graph Regex\n",
    "- Section Query presents how to describe different graph patterns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "- Python packages : aidge_core, matplotlib, numpy\n",
    "- Define model visualization function "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install --quiet matplotlib\n",
    "!pip install --quiet numpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import base64\n",
    "from IPython.display import Image, display\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def visualize_mmd(path_to_mmd):\n",
    "  with open(path_to_mmd, \"r\") as file_mmd:\n",
    "    graph_mmd = file_mmd.read()\n",
    "\n",
    "  graphbytes = graph_mmd.encode(\"ascii\")\n",
    "  base64_bytes = base64.b64encode(graphbytes)\n",
    "  base64_string = base64_bytes.decode(\"ascii\")\n",
    "  display(Image(url=f\"https://mermaid.ink/img/{base64_string}\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Graph Regex Flow\n",
    "![pipeline_graph_matching](./static/pipeline_gm.PNG)\n",
    "\n",
    "The GraphRegex class enable to perform graph transformations. It takes as inputs :\n",
    "- A GraphView\n",
    "- Queries : express one or more graph pattern descriptions to find in the GraphView\n",
    "\n",
    "To have more information on the query, i.e. graph descriptions, please refer to the following slides to open in your browser :\n",
    "- Basic concepts of [Graph Regular Expression](./static/GraphRegularExpression.html)\n",
    "- [Quantifier](./static/Quantifier.html)\n",
    "\n",
    "\n",
    "For more information, please refer to the User Guide on [graph transformation](https://eclipse-aidge.readthedocs.io/en/latest/source/UserGuide/transformGraph.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Query\n",
    "\n",
    "This section presents several examples to express different graph patterns : sequential, parallel, and advanced node testing.\n",
    "\n",
    "### Sequential graph \n",
    "- Create a sequential graph\n",
    "- Describe a pattern with graph Regular Expression\n",
    "- Match the describe pattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core\n",
    "\n",
    "model = aidge_core.sequential([aidge_core.MatMul(name=\"MatMul0\"),\n",
    "                                aidge_core.Add(2, name=\"Add0\"),\n",
    "                                aidge_core.ReLU(name=\"ReLU0\"),\n",
    "                                aidge_core.MatMul(name=\"MatMul1\"),\n",
    "                                aidge_core.Add(2, name=\"Add1\"),\n",
    "                                aidge_core.ReLU(name=\"ReLU1\")])\n",
    "\n",
    "model.save(\"mySequentialModel\")\n",
    "visualize_mmd(\"mySequentialModel.mmd\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph_regex = aidge_core.GraphRegex()\n",
    "graph_regex.set_node_key(\"A\", \"getType($) =='Add'\")\n",
    "graph_regex.set_node_key(\"B\", \"getType($) =='ReLU'\")\n",
    "graph_regex.add_query(\"A -> B\")\n",
    "\n",
    "all_match = graph_regex.match(model)\n",
    "print('Number of match : ', len(all_match))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, we have two matches. Each match contains :\n",
    "- The associated query. In this example : \"A->B\"\n",
    "- The list containing the start nodes. In this example : [Add0]\n",
    "- The set containing all the matched nodes. In this example : {Add0, ReLU0}\n",
    "\n",
    "Let's visualize the matches :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, match in enumerate(all_match):\n",
    "    print('Match ', i ,' associated to query : ', match.get_query())\n",
    "    print('The start node :', match.get_start_node()[0].name())\n",
    "    print('All the matched nodes :')\n",
    "    for n in match.get_all():\n",
    "        print('\\t', n.name())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parallel graph \n",
    "- Create a graph with parallel branches\n",
    "- Describe a pattern with graph Regular Expression\n",
    "- Match the describe pattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core \n",
    "\n",
    "model = aidge_core.sequential([aidge_core.MatMul(name=\"MatMul0\"),\n",
    "                                aidge_core.Add(2, name=\"Add0\"),\n",
    "                                aidge_core.ReLU(name=\"ReLU0\"),\n",
    "                                aidge_core.parallel([aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv0\"), aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv1\")]),\n",
    "                                aidge_core.Add(2, name=\"Add1\"),\n",
    "                                aidge_core.ReLU(name=\"ReLU1\")])\n",
    "\n",
    "model.save(\"mySequentialModel\")\n",
    "visualize_mmd(\"mySequentialModel.mmd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When applying regular expression concepts to graphs, we find ourselves dealing with parallel branches.\n",
    "To address this, we express the graph by decomposing it into sequences.\n",
    "This decomposition involves defining two types of nodes: \n",
    "- common nodes, which reference the same node across multiple sequences : token \"#\"\n",
    "- unique nodes, as the name suggests, that are exclusive to a particular sequence.\n",
    "\n",
    "Let's try to match the ReLU followed by the two parallel convolutions :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph_regex = aidge_core.GraphRegex()\n",
    "graph_regex.set_node_key(\"A\", \"getType($) =='ReLU'\")\n",
    "graph_regex.set_node_key(\"B\", \"getType($) =='Conv'\")\n",
    "graph_regex.add_query(\"A# -> B; A# -> B\")\n",
    "\n",
    "all_match = graph_regex.match(model)\n",
    "print('Number of match : ', len(all_match))\n",
    "\n",
    "for i, match in enumerate(all_match):\n",
    "    print('Match ', i ,' associated to query : ', match.get_query())\n",
    "    print('The start node :', match.get_start_node()[0].name())\n",
    "    print('All the matched nodes :')\n",
    "    for n in match.get_all():\n",
    "        print('\\t', n.name())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quantifiers\n",
    "\n",
    "Quantifiers are tokens that specify the number of occurrences of a preceding element in the pattern. The supported quantifiers in graph regular expressions are : \n",
    "- \"+\" : one or more\n",
    "- \"*\" : zero or more\n",
    "\n",
    "Using the quantifiers, the graph regular expression can represent an ensemble of patterns. \n",
    "For example \"Conv+\" represents sequences of at least one convolution. \n",
    "\n",
    "In this subsection :\n",
    "- Create a sequential graph\n",
    "- Describe a pattern with graph Regular Expression using quantifier 'one or more' : '+' \n",
    "- Match the describe pattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core\n",
    "\n",
    "model = aidge_core.sequential([aidge_core.MatMul(name=\"MatMul0\"),\n",
    "                                aidge_core.Add(2, name=\"Add0\"),\n",
    "                                aidge_core.ReLU(name=\"ReLU0\"),\n",
    "                                aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv0\"),\n",
    "                                aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv1\"),\n",
    "                                aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv2\")])\n",
    "\n",
    "model.save(\"mySequentialModel\")\n",
    "visualize_mmd(\"mySequentialModel.mmd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Let's try to match the ReLU0 followed by at least one convolution :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph_regex = aidge_core.GraphRegex()\n",
    "graph_regex.set_node_key(\"A\", \"getType($) =='ReLU'\")\n",
    "graph_regex.set_node_key(\"B\", \"getType($) =='Conv'\")\n",
    "graph_regex.add_query(\"A -> B+\")\n",
    "\n",
    "all_match = graph_regex.match(model)\n",
    "print('Number of match : ', len(all_match))\n",
    "\n",
    "for i, match in enumerate(all_match):\n",
    "    print('Match ', i ,' associated to query : ', match.get_query())\n",
    "    print('The start node :', match.get_start_node()[0].name())\n",
    "    print('All the matched nodes :')\n",
    "    for n in match.get_all():\n",
    "        print('\\t', n.name())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced node testing\n",
    "\n",
    "So far, the described graph patterns were based on the topology of the graph and the type of nodes.\n",
    "Graph Regular expression allows to perform any test on the node by supporting the function calls of the given node.\n",
    "\n",
    "- Create a sequential graph\n",
    "- Describe a pattern with graph Regular Expression using advanced testing on the dimension of the convolution kernels.\n",
    "- Match the describe pattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core\n",
    "\n",
    "conv = aidge_core.Conv2D(1, 1, [7, 7], name=\"C\")\n",
    "print(conv.get_operator().get_attr(\"KernelDims\") == [7,7])\n",
    "print(conv.type())\n",
    "\n",
    "\n",
    "model = aidge_core.sequential([aidge_core.Conv2D(1, 1, [7, 7], name=\"Conv0\"),\n",
    "                                aidge_core.Conv2D(1, 1, [5, 5], name=\"Conv1\"),\n",
    "                                aidge_core.Conv2D(1, 1, [3, 3], name=\"Conv2\"),\n",
    "                                aidge_core.Conv2D(1, 1, [5, 5], name=\"Conv3\"),\n",
    "                                aidge_core.Conv2D(1, 1, [7, 7], name=\"Conv4\")])\n",
    "\n",
    "model.save(\"mySequentialModel\")\n",
    "visualize_mmd(\"mySequentialModel.mmd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to find the convolutions with kernel size 3 or 5.\n",
    "\n",
    "First define your own custom testing fonctions :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_kernel_3(node):\n",
    "    b = False\n",
    "    if (node.type() == \"Conv\") :\n",
    "        b = node.get_operator().get_attr(\"KernelDims\") == [3,3]\n",
    "    return b\n",
    "\n",
    "def test_kernel_5(node):\n",
    "    b = False\n",
    "    if (node.type() == \"Conv\") :\n",
    "        b = node.get_operator().get_attr(\"KernelDims\") == [5,5]\n",
    "    return b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then register it in the graph regex and add the associated queries :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph_regex = aidge_core.GraphRegex()\n",
    "\n",
    "# Register the custom testing functions\n",
    "graph_regex.set_node_key(\"testk3\", test_kernel_3)\n",
    "graph_regex.set_node_key(\"testk5\", test_kernel_5)\n",
    "graph_regex.set_node_key(\"Convk3\", \"testk3($)==true\")\n",
    "graph_regex.set_node_key(\"Convk5\", \"testk5($)==true\")\n",
    "    \n",
    "#Add the associated queries\n",
    "graph_regex.add_query(\"Convk3\")\n",
    "graph_regex.add_query(\"Convk5\")\n",
    "\n",
    "all_match = graph_regex.match(model)\n",
    "print('Number of match : ', len(all_match))\n",
    "\n",
    "for i, match in enumerate(all_match):\n",
    "    print('Match ', i ,' associated to query : ', match.get_query())\n",
    "    print('The start node :', match.get_start_node()[0].name())\n",
    "    print('All the matched nodes :')\n",
    "    for n in match.get_all():\n",
    "        print('\\t', n.name())"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_aidge",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
