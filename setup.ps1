# Helper setup tool to automatically build aidge on Windows.

# Enable or disable automatic installation of requirements
# Run .\setup.ps1 -install_reqs:$false to disable it
param ([bool]$install_reqs=$true)

# Default install path is .\install_cpp
if (-not $env:AIDGE_INSTALL_PATH)
{
    $env:AIDGE_INSTALL_PATH = $(Join-Path $pwd/aidge install_cpp)
}

# aidge_core
Set-Location aidge/aidge_core
. .\setup.ps1 -install_reqs:$install_reqs
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
Set-Location $PSScriptRoot

# aidge_backend_cpu
Set-Location aidge/aidge_backend_cpu
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
. .\setup.ps1 -install_reqs:$install_reqs
Set-Location $PSScriptRoot
