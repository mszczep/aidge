Recipies
========

Fuse Mul Add
------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.fuse_mul_add

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::fuseMulAdd(std::shared_ptr<GraphView> graphView)

Remove flatten
--------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.remove_flatten

    .. tab-item:: C++

        .. doxygenfunction:: Aidge::removeFlatten(std::shared_ptr<GraphView> graphView)

