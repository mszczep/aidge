#!/usr/bin/env python3
""" Aidge

#TODO Temporary setup (to change with stable inplace code)
"""

DOCLINES = (__doc__ or '').split("\n")

import sys
import os

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
"""

import pathlib

from setuptools import setup, Extension
from setuptools import find_packages
from setuptools.command.build_ext import build_ext

def get_project_version() -> str:
    aidge_root = pathlib.Path().absolute()
    version = open(aidge_root / "version.txt", "r").read().strip()
    return version

def has_python_file(folder_path, package_name):
    # Create a Path object for the specified folder
    folder_path = pathlib.Path(folder_path)

    # Check if package_name exists in the folder
    return (folder_path / package_name).exists()


def install_aidge_package(module_name):
    # Check if the module has a setup.py
    if (not has_python_file(module_name, "setup.py")):
        raise RuntimeError("Setup.py file required to install this module")

    # Check if the module has a requirements file
    # If, so run it
    if(has_python_file(module_name, "requirements.txt")):
        os.system(f"{sys.executable} -m pip install -r {module_name / 'requirements.txt'}")

    # Install the module in the user python environment
        os.system(f"{sys.executable} -m pip install {module_name} -v")



class CMakeExtension(Extension):
    def __init__(self, name):
        super().__init__(name, sources=[])

class CMakeBuild(build_ext):

    def run(self):

        # Old fashioned way to install Aidge bundle
        # self.spawn(["./setup.sh", "--pybind", "--python"])

        aidge_root = pathlib.Path().absolute()

        # Check if the aidge bundle have a requirements file
        # If, so run it
        if(has_python_file(aidge_root, "requirements.txt")):
            os.system(f"{sys.executable} -m pip install -r {aidge_root / 'requirements.txt'}")

        aidge_bundle_path = aidge_root / "aidge"

        # Install all libraries managed by the bundle
        install_aidge_package(aidge_bundle_path / "aidge_core")
        install_aidge_package(aidge_bundle_path / "aidge_backend_cpu")
        install_aidge_package(aidge_bundle_path / "aidge_export_cpp")
        install_aidge_package(aidge_bundle_path / "aidge_onnx")


if __name__ == '__main__':

    setup(
        name="aidge",
        version=get_project_version(),
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        packages=find_packages(where="."),
        include_package_data=True,
        ext_modules=[CMakeExtension("aidge")],
        cmdclass={
            'build_ext': CMakeBuild,
        },
        zip_safe=False,

    )
